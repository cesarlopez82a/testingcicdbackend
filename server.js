'use strict';

const express = require('express');

// Constants
const PORT = 8080;
const HOST = '0.0.0.0';

// App
const app = express();
app.get('/', (req, res) => {
  res.send('Hello World desde el backend!');
});
app.get('/saludo2', (req, res) => {
  res.send('Hello World desde el backend - saludos2222!!!');
});
app.get('/saludo3', (req, res) => {
  res.send('SALUDOOOOO!!!');
});
app.get('/saludo4', (req, res) => {
  res.send('SALUDO 4!!!');
});
app.get('/saludo5', (req, res) => {
  res.send('SALUDO 33333333333333333333355!!!');
});



app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);
